/*
 *	Empfänger
 *	Haus
 *  - WLAN-Client
 *  - MQTT
 *  - NTP
 *  - Display
 */

/**************/
/*	Includes  */
/**************/
// LoRaWan
#include <SPI.h>
#include <LoRa.h>
#include <string.h>
// WLAN
#include <WiFi.h>
#include "credentials.h"  // See 'credentials' tab and enter your OWM API key and set the Wifi SSID and PASSWORD
// NTP
#include <time.h>
// MQTT
#include <PubSubClient.h>
// SDA/SDL (BME und Display)
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

/****************/
/*	Konstanten  */
/****************/
// LoRaWan PINS definieren
#define Pnss 5		// NSS
#define Preset 14	// Reset
#define Pdio0 2		// DIO0
// WLAN-Zugang
// Zugangsdaten in der credentials.h pflegen!
// MQTT-Server (Openhab)
#define mqtt_server "192.168.2.25"
#define mqtt_user "openhabian"
#define mqtt_password ""
// NTP-Client
const char* ntpServer = "192.168.2.1";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
// MQTT-Topics
// BME280
#define topic_temperature "garage/temperature"
#define topic_humidity "garage/humidity"
#define topic_pressure "garage/pressure"
#define topic_dewpoint "garage/dewpoint"
#define topic_heatindex "garage/heatindex"
// IR-LED Stromzähler
#define topic_energy_daily "garage/strom_tagesstand"
#define topic_energy_diff "garage/strom_diff"
// Shellys
#define topic_shelly_1 "garage/shelly_freigabe"
#define topic_shelly_2 "garage/shelly_torauf"
#define topic_shelly_3 "garage/shelly_torzu"
// Connections
WiFiClient espClient;
PubSubClient client(espClient);
// Display
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

/***************/
/*	Variablen  */
/***************/
// LoRaWan
String LoRaData, ID, EnergyMinute, EnergyDay, Temperature, Humidity, Luftdruck, Shelly1, Shelly2, Shelly3 = "";
char* LoRaSplit[10];
int LoRaLoop;
int LoRaSucc;
String LoRaSuccTime;
// NTP
struct tm timeinfo;
// WLAN
int wifi_signal;

void setup() {
  // Serieller Monitor
  Serial.begin(115200);
  Serial.println("Empfänger Haus");

/*
 * Display
 */
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  Serial.println("Display gestartet");
  delay(2000);
  display.clearDisplay();
  display.setTextColor(WHITE);

/*
 * WLAN & MQTT
 */
  StartWiFi();
  client.setServer(mqtt_server, 1883);
  reconnect();
  
/*
 * LoRaWan-Einstellungen
 */
  // Setup Modul
  LoRa.setPins(Pnss, Preset, Pdio0);
  // Frequenz im LoRa.begin() eintragen:
  // Asien: 		433E6
  // Nord America:	915E6
  // Europa:		866E6
  while (!LoRa.begin(866E6)) {
    Serial.println("-");
    delay(500);
  }
  // Sync-Wort im LoRa.setSyncWord() eintragen.
  // Das Sync-Wort ist keine Verschlüsselung sondern nur eine gemeinsame Konvention.
  // Errät jemand das Sync-Wort kann er die Nachrichten ebenfalls empfangen, bzw. Nachrichten senden
  // Muss in beiden Geräten gleich sein (von 0-0xFF)
  LoRa.setSyncWord(0x3F);
  Serial.println("LoRa gestartet");
  LoRaLoop = 0;

/*
 * NTP
 */ 
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

}

void loop() {
  client.loop();
/*
 *  LoRaWan Receiver immer laufen lassen
 *  um Pakete zu empfangen
 */
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // Paket empfangen
    Serial.print("Paket empfangen: '");
    // read packet
    while (LoRa.available()) {
      LoRaData = LoRa.readString();
      Serial.print(LoRaData); 
    }
    // LoRaLoop => Paket empfangen
    LoRaLoop++;
    // print RSSI of packet
    Serial.print("' mit RSSI ");
    Serial.println(LoRa.packetRssi());
    if (!client.connected()) {
      reconnect();
    }
    client.publish("garage/string", LoRaData.c_str(), true);
    //counter + "#" + EnergyMinute + "#" + EnergyDay + "#" + Temperature + "#" + Humidity + "#" + Luftdruck + "#" + String(Shelly1_Status) + "#" + String(Shelly2_Status) + "#" + String(Shelly3_Status);
    char str_array[LoRaData.length()];
    LoRaData.toCharArray(str_array, LoRaData.length());

    client.loop();

    char *token = strtok(str_array, "#");
    int loopcounter = 0;
    while (token != NULL) {
      LoRaSplit[loopcounter] = token;
      token = strtok(NULL, "#");
      loopcounter++;
    }
    
    // Variablen auf plausibilität prüfen und sonst verwerfen!
    // 235#1#42177#10.33#100.00#982.34#0#0#1#
    // Variablen füllen
    if (isNumeric(LoRaSplit[0]) && isNumeric(LoRaSplit[1]) && isNumeric(LoRaSplit[2]) && isNumeric(LoRaSplit[3]) && isNumeric(LoRaSplit[4]) && isNumeric(LoRaSplit[5]) && isNumeric(LoRaSplit[6]) && isNumeric(LoRaSplit[7]) && isNumeric(LoRaSplit[8])) { 
      ID = LoRaSplit[0]; 
      EnergyMinute = LoRaSplit[1];
      EnergyDay = LoRaSplit[2]; 
      Temperature = LoRaSplit[3]; 
      Humidity = LoRaSplit[4]; 
      Luftdruck = LoRaSplit[5]; 
      Shelly1 = LoRaSplit[6]; 
      Shelly2 = LoRaSplit[7]; 
      Shelly3 = LoRaSplit[8];
      // LoRaSucc => Paket komplett
      LoRaSucc++;

      if(!getLocalTime(&timeinfo)){
        Serial.println("Failed to obtain time");
        return;
      }
      char timeHour[3];
      strftime(timeHour,3, "%H", &timeinfo);
      char timeMinute[3];
      strftime(timeMinute,3, "%M", &timeinfo);
      LoRaSuccTime = String(timeHour) + ":" + String(timeMinute);
      Serial.println(LoRaSuccTime);

      if (timeHour == 0 && timeMinute == 0) {
        LoRaSucc = 0;
        LoRaLoop = 0;
      }
    }
  
    // Taupunkt und Hizeidex rechnen
    float bme_temp = Temperature.toFloat();
    float bme_humidity = Humidity.toFloat();
    float bme_pressure = Luftdruck.toFloat();
    float T = (bme_temp * 9 / 5) + 32;           // Convert back to deg-F for the RH equation
    float RHx = bme_humidity;                    // Short form of RH for inclusion in the equation makes it easier to read
    float heat_index = (-42.379+(2.04901523*T)+(10.14333127*RHx)-(0.22475541*T*RHx)-(0.00683783*sq(T))-(0.05481717*sq(RHx))+(0.00122874*sq(T)*RHx)+(0.00085282*T*sq(RHx))-(0.00000199*sq(T)*sq(RHx))-32)*5/9;
    if ((bme_temp <= 26.66) || (bme_humidity <= 40)) heat_index = bme_temp; // The convention is not to report heat Index when temperature is < 26.6 Deg-C or humidity < 40%
    float dew_point = 243.04*(log(bme_humidity/100)+((17.625*bme_temp)/(243.04+bme_temp)))/(17.625-log(bme_humidity/100)-((17.625*bme_temp)/(243.04+bme_temp)));
/*
 * MQTT
 */
    client.loop();

    if (!client.connected()) {
      reconnect();
    }
    client.publish("garage/ID", ID.c_str(), true);
    client.publish(topic_temperature, String(bme_temp).c_str(), true);
    client.publish(topic_humidity, String(bme_humidity).c_str(), true);
    client.publish(topic_pressure, String(bme_pressure).c_str(), true);
    client.publish(topic_dewpoint, String(dew_point).c_str(), true);
    client.publish(topic_heatindex, String(heat_index).c_str(), true);
    client.publish(topic_energy_daily, EnergyDay.c_str(), true);
    client.publish(topic_energy_diff, EnergyMinute.c_str(), true);
    client.publish(topic_shelly_1, Shelly1.c_str(), true);
    client.publish(topic_shelly_2, Shelly2.c_str(), true);
    client.publish(topic_shelly_3, Shelly3.c_str(), true);
    client.loop();
  }
  
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print(String(LoRaSucc) + "/" + String(LoRaLoop));
  display.setTextSize(1);
  display.setCursor(0,20);
  display.print(ID + " | " + LoRaSuccTime + " | " + String(LoRa.packetRssi()));
  display.setCursor(0,35);
  display.print("WiFi: " + String(wl_status_to_string(WiFi.status())));
  display.setCursor(0,50);
  display.print("MQTT: " + String(mqtt_status_to_string(client.state())));
  display.display();

}

const char* wl_status_to_string(wl_status_t status) {
  switch (status) {
    case WL_NO_SHIELD: return "WL_NO_SHIELD";
    case WL_IDLE_STATUS: return "WL_IDLE_STATUS";
    case WL_NO_SSID_AVAIL: return "WL_NO_SSID_AVAIL";
    case WL_SCAN_COMPLETED: return "WL_SCAN_COMPLETED";
    case WL_CONNECTED: return "WL_CONNECTED";
    case WL_CONNECT_FAILED: return "WL_CONNECT_FAILED";
    case WL_CONNECTION_LOST: return "WL_CONNECTION_LOST";
    case WL_DISCONNECTED: return "WL_DISCONNECTED";
  }
}

const char* mqtt_status_to_string(int status) {
  switch (status) {
    case -4: return "MQTT_CONNECTION_TIMEOUT"; // - the server didn't respond within the keepalive time
    case -3: return "MQTT_CONNECTION_LOST"; // - the network connection was broken
    case -2: return "MQTT_CONNECT_FAILED"; // - the network connection failed
    case -1: return "MQTT_DISCONNECTED"; // - the client is disconnected cleanly
    case 0: return "MQTT_CONNECTED"; // - the client is connected
    case 1: return "MQTT_CONNECT_BAD_PROTOCOL"; // - the server doesn't support the requested version of MQTT
    case 2: return "MQTT_CONNECT_BAD_CLIENT_ID"; // - the server rejected the client identifier
    case 3: return "MQTT_CONNECT_UNAVAILABLE"; // - the server was unable to accept the connection
    case 4: return "MQTT_CONNECT_BAD_CREDENTIALS"; // - the username/password were rejected
    case 5: return "MQTT_CONNECT_UNAUTHORIZED"; // - the client was not authorized to connect
  }
}

uint8_t StartWiFi() {
  Serial.print("\r\nConnecting to: ");
  Serial.println(String(ssid));

  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("\r\nConnecting to: ");
  display.print(String(ssid));
  display.display();

  IPAddress dns(8, 8, 8, 8); // Google DNS
  WiFi.disconnect();
  WiFi.mode(WIFI_STA); // switch off AP
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.scanNetworks();
  WiFi.begin(ssid, password);
  unsigned long start = millis();
  uint8_t connectionStatus;
  bool AttemptConnection = true;
  while (AttemptConnection) {
    connectionStatus = WiFi.status();
    if (millis() > start + 15000) { // Wait 15-secs maximum
      AttemptConnection = false;
    }
    if (connectionStatus == WL_CONNECTED || connectionStatus == WL_CONNECT_FAILED) {
      AttemptConnection = false;
    }
    delay(50);
  }
  if (connectionStatus == WL_CONNECTED) {
    wifi_signal = WiFi.RSSI(); // Get Wifi Signal strength now, because the WiFi will be turned off to save power!
    Serial.println("WiFi connected at: " + WiFi.localIP().toString());
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(0,0);
    display.print("WiFi connected at: " + WiFi.localIP().toString());
    display.display();
  }
  else {
    Serial.println("WiFi connection *** FAILED ***");
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(0,0);
    display.print("WiFi connection *** FAILED ***");
    display.display();
  }
  
  return connectionStatus;
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32-LoRaWanGarageReceiver";
    // Attempt to connect
    client.setKeepAlive(130);  
    if (client.connect(clientId.c_str(), mqtt_user, mqtt_password)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "LoRaWan Haus alive");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 0.5 seconds");
        // Wait 5 seconds before retrying
        delay(500);
      }
   }
}

boolean isNumeric(String str) {
  str.trim(); // line added: eliminate spaces, and \n before and after the string
  unsigned int stringLength = str.length();

  if (stringLength == 0) {
    return false;
  }
  boolean seenDecimal = false;
  for(unsigned int i = 0; i < stringLength; ++i) {
    if (i==0 && (str.charAt(0)=='+' || str.charAt(0)=='-')) // handle the "+" and "-" caracters only on the first place
      continue;
    if (isDigit(str.charAt(i))) {
      continue;
    }
    if (str.charAt(i) == '.') {
      if (seenDecimal) {
        return false;
      }
      seenDecimal = true;
      continue;
    }
    return false;
  }
  return true;
}
