/*
 *	Sender
 *	Garage
 *	- BME280
 *  - Display
 *  - IR-Diode
 *	- WLAN-AP
 *  - Shelly-Abfrage
 */

/**************/
/*	Includes  */
/**************/
// SDA/SDL (BME und Display)
#include <Wire.h>
#include "RTClib.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <HTU21D.h>
// LoRaWan
#include <SPI.h>
#include <LoRa.h>
// WLAN
#include <WiFi.h>
#include "credentials.h"  // See 'credentials' tab and enter your OWM API key and set the Wifi SSID and PASSWORD
// Shelly
#include <HTTPClient.h>
HTTPClient http;

/****************/
/*	Konstanten  */
/****************/
// LoRaWan PINS definieren
#define Pnss 5		// NSS
#define Preset 14	// Reset
#define Pdio0 2		// DIO0
// Temperatursensor
HTU21D htusensor;
// Display
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
// RTC
RTC_DS3231 rtc;
// IR-Diode Stromzähler PIN definieren
int Pin_powermeter = 16; // GPIO 16
// WLAN
// Zugangsdaten in der credentials.h pflegen!
IPAddress local_IP(192,168,3,1);
IPAddress gateway(192,168,3,1);
IPAddress subnet(255,255,255,0);
IPAddress IP;  // Host-IP
//Shellys
String Shelly_IP_Freigabe	= "192.168.3.20";
String Shelly_IP_TorAuf		= "192.168.3.21";
String Shelly_IP_TorZu		= "192.168.3.22";
String Shelly1_Raw, Shelly1_Status, Shelly2_Raw, Shelly2_Status, Shelly3_Raw, Shelly3_Status;

/***************/
/*	Variablen  */
/***************/
// LoRaWan
int counter = 1;					// Zähler für den Sendevorgang
String LoRaData;					// Zu sendende Daten
// Temperatur
String Temperature = "0";
String Humidity = "0";
String Luftdruck = "0"; // vorerst aber immer 0
// Globales
int lastMinute = 0;
int EnergyImpLast = 0;     			// Energie-Impuls im letzten Loop
int EnergyDay = 0; 					// Tageswert
int EnergyMinute = 0; 				// Minutenwert

void setup() {
  // Serieller Monitor
  Serial.begin(115200);
  Serial.println("Sender Garage");

/*
 * Display
 */
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  Serial.println("Display gestartet");
  delay(2000);
  display.clearDisplay();
  display.setTextColor(WHITE);
  
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Sender Garage");
  display.display();

/*
 * LoRaWan-Einstellungen
 */
  // Setup Modul
  LoRa.setPins(Pnss, Preset, Pdio0);
  // Frequenz im LoRa.begin() eintragen:
  // Asien: 		433E6
  // Nord America:	915E6
  // Europa:		866E6
  //display.setCursor(0,10);
  //display.print("LoRa startet: ");
  while (!LoRa.begin(866E6)) {
    Serial.println("-");
    delay(500);
    //display.print("-");
    //display.display();
  }
  // Sync-Wort im LoRa.setSyncWord() eintragen.
  // Das Sync-Wort ist keine Verschlüsselung sondern nur eine gemeinsame Konvention.
  // Errät jemand das Sync-Wort kann er die Nachrichten ebenfalls empfangen, bzw. Nachrichten senden
  // Muss in beiden Geräten gleich sein (von 0-0xFF)
  LoRa.setSyncWord(0x3F);
  Serial.println("LoRa gestartet");
  //display.setCursor(0,20);
  //display.print("LoRa gestartet");
  //display.display();

/*
 * Temperatur
 */
  htusensor.begin();
  display.setCursor(0,15);
  display.print("HTU gestartet");
  display.display();

/*
 * RTC
 */

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1) delay(10);
  }
  rtc.begin();
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  display.setCursor(0,30);
  display.print("RTC gestartet");
  display.display();

/*
 * IR-Diode Stromzähler
*/
  pinMode (Pin_powermeter, INPUT);
  
/*
 * WLAN-AP
 */
  WiFi.softAPConfig(local_IP, gateway, subnet);
  WiFi.softAP(ssid, password);
  Serial.println("AP startet");

  IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
  //display.setCursor(0,40);
  //display.print("WLAN gestartet");
  //display.setCursor(0,50);
  //display.print("IP: " + String(IP));
  //display.display();

}

void loop() {
/*
 *  LoRaWan Receiver immer laufen lassen
 *  um Pakete zu empfangen
 */
  DateTime now = rtc.now();
  if (now.hour() == 0 && now.minute() == 0 && now.second() == 0 && counter > 2) {
    // Um 0 Uhr soll der interne Zähler 
    // zurückgesetzt werden da ein neuer Tag begonnen hat
    counter = 1;
    // Ebenfalls wird der Tagesstromzähler zurückgesetzt
    EnergyDay = 0;
  }

/*
 * IR-Diode Stromzähler
 */
  // Energiezähler
  // Bei jeder steigenden Flanke einen Impuls auf den
  // Minuten- und den Tageszähler addieren
  int EnergyImpNow = digitalRead (Pin_powermeter);
  if(EnergyImpNow==1 && EnergyImpNow!=EnergyImpLast) {
      // Zählimpuls
      EnergyDay++;
      EnergyMinute++;
  }
  if (EnergyImpNow!=EnergyImpLast) {
    EnergyImpLast = EnergyImpNow;
  }

/*
 * Sonstiges, nicht so zeitkritisches
 *  - Sensordaten
 *  - Display
 *  - Shellys
 *  - LoRaWan Sender
 *  nur im definierten Intervall
 */
  //Serial.println(String(lastMinute) + " " + String(now.minute()));
  if (now.minute() != lastMinute) {
    lastMinute = now.minute();
    Serial.println(String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()));
    
/*
 * Shellys abfragen
 */

    Shelly1_Raw = fetchShelly("http://" + Shelly_IP_Freigabe + "/rpc/Input.GetStatus?id=0");
    Shelly2_Raw = fetchShelly("http://" + Shelly_IP_TorAuf + "/rpc/Input.GetStatus?id=0");
    Shelly3_Raw = fetchShelly("http://" + Shelly_IP_TorZu + "/rpc/Input.GetStatus?id=0");
/*
    Shelly1_Raw = "{\"id\":0,\"state\":true}";
    Shelly2_Raw = "{\"id\":0,\"state\":false}";
    Shelly3_Raw = "{\"id\":0,\"state\":false}";
*/
    if (Shelly1_Raw.substring(16, 17)=="t") { Shelly1_Status = "1"; } else { Shelly1_Status = "0"; }
    if (Shelly2_Raw.substring(16, 17)=="t") { Shelly2_Status = "1"; } else { Shelly2_Status = "0"; }
    if (Shelly3_Raw.substring(16, 17)=="t") { Shelly3_Status = "1"; } else { Shelly3_Status = "0"; }
    
/*
 * Temperatursensor
 */
    if(htusensor.measure()) {
      Temperature = String(htusensor.getTemperature());
      Humidity = String(htusensor.getHumidity());
    }

/*
 * Display
 */
//	  String Zeile1 = "Temperature: " + Temperature + " C";
//	  String Zeile2 = "Humidity:    " + Humidity + " %";
//	  String Zeile3 = "Luftdruck    " + Luftdruck + " mbar";
//	  String Zeile4 = "Energy:      " + String(EnergyMinute) + " Imp/Min";
//	  String Zeile5 = "ID:          " + String(counter);
//	  String Zeile6 = "S1: " + String(Shelly1_Status) + "   S2: " + String(Shelly2_Status) + "   S3: " + String(Shelly3_Status);

	  display.clearDisplay();
    display.setTextColor(WHITE); 

    display.setTextSize(3);
    display.setCursor(0,0);
    display.print(Temperature.toFloat(), 0);
    display.setTextSize(2);
    display.setCursor(30,6);
    display.print(" ");
    display.setTextSize(1);
    display.cp437(true);
    display.write(167);
    display.setTextSize(2);
    display.print("C");

    display.setTextSize(3);
    display.setCursor(70,0);
    display.print(Humidity.toFloat(), 0);
    display.setTextSize(2);
    display.setCursor(100,6);
    display.print(" %");

    display.setTextSize(3);
    display.setCursor(0,29);
    display.print((EnergyDay/10000), 1);
    display.setTextSize(2);
    display.setCursor(65,35);
    display.print(" kWh");

    display.setTextSize(1);
    display.setCursor(0,53);
    display.print(String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()) + " | " + counter);
    display.display();

/*
 * LoRaWan
 */
    String LoRaString = String(counter) + "#" + String(EnergyMinute) + "#" + String(EnergyDay) + "#" + Temperature + "#" + Humidity + "#" + Luftdruck + "#" + Shelly1_Status + "#" + Shelly2_Status + "#" + Shelly3_Status + "#";
    Serial.print("Sende Paket: ");
    Serial.println(LoRaString);

    LoRa.beginPacket();
    LoRa.print(LoRaString);
    LoRa.endPacket();
    
    EnergyMinute = 0;
    counter++;
  }

}

String fetchShelly(String url) {
  String serverPath = url;
  String payload = "";
  // Your Domain name with URL path or IP address with path
  http.begin(serverPath.c_str());
  http.setConnectTimeout(1000);
  // Send HTTP GET request
  int httpResponseCode = http.GET();
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
    Serial.println(payload);
  }
  else {
    //Serial.print("Error code: ");
    //Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();
  return payload;
}
